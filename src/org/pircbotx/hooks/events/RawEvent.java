package org.pircbotx.hooks.events;

import org.pircbotx.PircBotX;
import org.pircbotx.hooks.Event;

public class RawEvent<T extends PircBotX> extends Event<T> {

    private final String line;

    /**
     * Constructs a RawEvent which contains raw information from IRC
     *
     * @param message received directly from the IRC server
     */
    public RawEvent(T bot, String message) {
        super(bot);
        line = message;
    }

    public String getMessage() {
        return line;
    }

    @Override
    public void respond(String response) {
        getBot().sendRawLineNow(response);
    }

}
