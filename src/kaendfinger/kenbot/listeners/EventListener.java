package kaendfinger.kenbot.listeners;

import org.pircbotx.hooks.Event;
import org.pircbotx.hooks.ListenerAdapter;

import java.lang.reflect.Method;
import java.util.HashMap;

public class EventListener extends ListenerAdapter {

    public static final String eventPrefix = "org.pircbotx.hooks.events.";
    private static HashMap<String, HashMap<String, Method>> events = new HashMap<>();

    public static boolean eventExists(String eventName, String handlerName) {
        if (!events.containsKey(eventName))
            events.put(eventName, new HashMap<String, Method>());
        return events.get(eventName).containsKey(handlerName);
    }

    public static void addEvent(String eventName, String handlerName, Method method) {
        if (eventExists(eventName, method.getName()))
            throw new RuntimeException(String.format("Event %s is already registered", method.getName()));
        events.get(eventName).put(handlerName, method);
    }

    public static void removeEvent(String eventName, String handlerName) {
        if (!eventExists(eventName, handlerName))
            throw new RuntimeException(String.format("Attempt to unregister a non-existant event %s", handlerName));
        events.get(eventName).remove(handlerName);
    }

    public static void postEvent(String eventName, Object event) throws Exception {
        if (!events.containsKey(eventName))
            events.put(eventName, new HashMap<String, Method>());
        for (Method m : events.get(eventName).values()) {
            m.invoke(null, event);
        }

    }

    @Override
    public void onEvent(Event event) throws Exception {
        postEvent(event.getClass().getName(), event);
    }
}