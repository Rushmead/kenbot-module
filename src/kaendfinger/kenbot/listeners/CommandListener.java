package kaendfinger.kenbot.listeners;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.core.KenBotConfig;
import kaendfinger.kenbot.core.UserCache;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.User;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.MessageEvent;
import org.pircbotx.hooks.events.PrivateMessageEvent;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class CommandListener extends ListenerAdapter {

    private static HashMap<String, Method> commands = new HashMap<>();

    public static Map<String, Method> getCommands() {
        return Collections.unmodifiableMap(commands);
    }

    public static boolean commandExists(String name) {
        return commands.containsKey(name);
    }

    public static void addCommand(Method method) {
        if (commandExists(method.getName()))
            throw new RuntimeException(String.format("Command %s is already registered", method.getName()));
        commands.put(method.getName(), method);
    }

    public static void removeCommand(String name) {
        if (!commandExists(name))
            throw new RuntimeException(String.format("Attempt to unregister a non-existant command %s", name));
        commands.remove(name);
    }

    @Override
    public void onMessage(MessageEvent event) {
        String message = event.getMessage();
        String prefixChar = KenBotConfig.get("prefix." + event.getChannel().getName());
        if (prefixChar.isEmpty())
            prefixChar = KenBotConfig.get("prefix");

        if (!(message.startsWith(prefixChar) && (message.length() > 1)))
            return;

        if (!registered(event.getUser()))
            return;

        String command;
        int space = message.indexOf(" ");
        if (space == -1)
            command = message.substring(1);
        else
            command = message.substring(1, space);

        if (!commandExists(command)) {
            if (KenBotConfig.get("verbose.invalid.command").equalsIgnoreCase("true"))
                event.getBot().sendNotice(event.getUser(), "Error: \"" + command + "\" is not a valid command; please see help");
            return;
        }

        if (!BotUtils.hasPermission(command, event.getChannel().getName(), event.getUser().getNick())) {
            if (KenBotConfig.get("verbose.invalid.permission").equalsIgnoreCase("true"))
                event.getBot().sendNotice(event.getUser(), "Error: lacking permissions to use this command");
            return;
        }
        executeCommand(event, command);

    }

    public void onPrivateMessage(PrivateMessageEvent event) {
        String message = event.getMessage();
        String prefixChar = KenBotConfig.get("prefix");

        if (message.startsWith(prefixChar) && (message.length() > 1))
            message = message.substring(0);

        if (!registered(event.getUser()))
            return;

        String command;
        int space = message.indexOf(" ");
        if (space == -1)
            command = message.substring(0);
        else
            command = message.substring(0, space);

        if (!commandExists(command)) {
            if (KenBotConfig.get("verbose.invalid.command").equalsIgnoreCase("true"))
                event.getBot().sendNotice(event.getUser(), "Error: \"" + command + "\" is not a valid command; please see help");
            return;
        }

        if (!BotUtils.hasPermission(command, null, event.getUser().getNick())) {
            if (KenBotConfig.get("verbose.invalid.permission").equalsIgnoreCase("true"))
                event.getBot().sendNotice(event.getUser(), "Error: lacking permissions to use this command");
            return;
        }

        try {
            Class clazz = Class.forName("org.pircbotx.Channel");

            Constructor constructor = clazz.getDeclaredConstructor(PircBotX.class, String.class);
            constructor.setAccessible(true);

            Channel instance = (Channel) constructor.newInstance(event.getBot(), event.getUser().getNick());
            executeCommand(new MessageEvent(event.getBot(), instance, event.getUser(), event.getMessage()), command);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void executeCommand(MessageEvent event, String command) {
        try {
            Method m = commands.get(command);
            if (m.getParameterTypes().length == 1) {
                commands.get(command).invoke(null, event);
            } else if (m.getParameterTypes().length == 2) {
                String[] args = getArgs(event.getMessage());
                Command annotation = commands.get(command).getAnnotation(Command.class);

                if (argCheck(args, annotation.minArgs(), annotation.maxArgs())) {
                    commands.get(command).invoke(null, event, args);
                } else {
                    if (!annotation.usage().isEmpty())
                        BotUtils.getBot().sendNotice(event.getUser(), "Usage: " + annotation.usage());
                    else
                        BotUtils.getBot().sendNotice(event.getUser(), "No usage available for " + command);
                }
            } else {
                System.out.println(String.format("%s has a method %s with to many parameters", m.getDeclaringClass().getName(), m.getName()));
            }
        } catch (Exception e) {
            System.out.println("Error: Unable to execute command " + command);
            BotUtils.getBot().sendNotice(event.getUser(), "There has been a severe command execution error, unable to execute");
            e.printStackTrace();
        }
    }

    private static boolean argCheck(String[] args, int min, int max) {
        if ((min == -1) && (max == -1))
            return true;

        if ((min != -1) && (max != -1)) // check min and max
            return ((args.length <= max) && (args.length >= min));

        if (min != -1) // check for min only
            return (args.length >= min);

        return (args.length <= max); // check max only
    }

    private static String[] getArgs(String message) {
        String[] args = message.split(" ");
        String[] newArgs = new String[args.length - 1];
        for (int i = 1; i < args.length; i++)
            newArgs[i - 1] = args[i].trim();
        return newArgs;
    }

    private static boolean registered(User user) {
        if ((KenBotConfig.get("force.registered").equalsIgnoreCase("true")) && (UserCache.isCached(user.getNick())) && (UserCache.getNickServName(user.getNick()) == null)) {
            BotUtils.getBot().sendNotice(user, "Error: you need to be logged in to use commands");
            return false;
        }
        return true;
    }

}
