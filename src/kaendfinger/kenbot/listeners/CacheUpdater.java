package kaendfinger.kenbot.listeners;

import kaendfinger.kenbot.api.registry.EventListener;
import kaendfinger.kenbot.core.UserCache;
import org.pircbotx.hooks.events.NickChangeEvent;
import org.pircbotx.hooks.events.PartEvent;

public class CacheUpdater {

    @EventListener(name = "DisconnectHandler", event = "PartEvent")
    public static void disconnectHandler(PartEvent event) {
        UserCache.removeMapping(event.getUser().getNick());
    }

    @EventListener(name = "NickChangeHandler", event = "NickChangeEvent")
    public static void nickChangeHandler(NickChangeEvent event) {
        if (UserCache.getNickServName(event.getOldNick()) == null)
            return;
        String nickServName = UserCache.getNickServName(event.getOldNick());
        UserCache.removeMapping(event.getOldNick());
        UserCache.addMapping(event.getNewNick(), nickServName);
    }

}
