package kaendfinger.kenbot.gui;

import kaendfinger.kenbot.api.Log;
import kaendfinger.kenbot.api.utils.BotUtils;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MenuBar extends JMenuBar {

    public MenuBar() {
        add(fileMenu());
    }

    private JMenu fileMenu() {
        JMenu menu = new JMenu("File");

        JMenuItem connect = new JMenuItem("Connect");
        connect.addActionListener(new ConnectListener());

        JMenuItem disconnect = new JMenuItem("Disconnect");
        disconnect.addActionListener(new DisconnectListener());

        menu.add(connect);
        menu.add(disconnect);

        return menu;
    }

    private static class ConnectListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                BotUtils.connect();
                System.out.println("Connection to server was successful");
                BotUtils.join();
            } catch (Exception ex) {
                Log.log("Unable to connect to IRC");
            }
        }
    }

    private static class DisconnectListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            try {
                BotUtils.disconnect();
                Footer.updateChatBox();
                System.out.println("Connection to server was successfully terminated");
            } catch (Exception ex) {
                System.out.println("Already disconnected from IRC");
            }
        }
    }
}
