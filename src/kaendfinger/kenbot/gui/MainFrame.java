package kaendfinger.kenbot.gui;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {

    public MainFrame() {
        super("KenBot GUI");
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setSize(600, 200);
        this.setResizable(true);
        this.setJMenuBar(new MenuBar());

        this.add(new TextArea());
        this.add(new Footer(), BorderLayout.SOUTH);

        this.pack();
        setVisible(true);
    }
}
