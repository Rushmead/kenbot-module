package kaendfinger.kenbot.gui;

import javax.swing.*;

public class TextArea extends JPanel {

    public static JTextArea textArea;

    public TextArea() {
        textArea = new JTextArea(7, 53);
        textArea.setEditable(false);
        this.add(textArea);

        JScrollPane scroll = new JScrollPane(textArea);
        this.add(scroll);
    }

}
