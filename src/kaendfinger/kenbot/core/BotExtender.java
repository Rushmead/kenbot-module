package kaendfinger.kenbot.core;

import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.gui.Footer;
import kaendfinger.kenbot.listeners.EventListener;
import org.pircbotx.Channel;
import org.pircbotx.PircBotX;
import org.pircbotx.exception.IrcException;
import org.pircbotx.hooks.events.RawEvent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class BotExtender extends PircBotX {

    private static ArrayList<String> channels = new ArrayList<>();

    @Override
    public void connect(String hostname) throws IOException, IrcException {
        inputThread = null; // make sure its dead before attempting to connect
        super.connect(hostname);
        String pass = KenBotConfig.get("password");
        if (!pass.isEmpty())
            BotUtils.getBot().identify(pass);
    }

    @Override
    public void disconnect() {
        super.disconnect();
        channels.removeAll(channels);
    }

    @Override
    public Set<String> getChannelsNames() {
        return Collections.unmodifiableSet(new HashSet<String>() {
            {
                for (String chan : channels)
                    add(chan);
            }
        });
    }

    @Override
    public void joinChannel(String channel) {
        super.joinChannel(channel);
        channels.add(channel);
        Footer.updateChatBox();
    }

    @Override
    public void partChannel(Channel channel) {
        super.partChannel(channel);
        channels.remove(channel.getName());
        Footer.updateChatBox();
    }

    @Override
    public void handleLine(String line) {
        try {
            super.handleLine(line);
            EventListener.postEvent(RawEvent.class.getName(), new RawEvent(BotUtils.getBot(), line));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
