package kaendfinger.kenbot.core;

import kaendfinger.kenbot.api.Log;
import kaendfinger.kenbot.api.registry.BotRegistry;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.commands.AdminCommands;
import kaendfinger.kenbot.commands.ConfigCommands;
import kaendfinger.kenbot.commands.ModuleCommands;
import kaendfinger.kenbot.commands.UserCommands;
import kaendfinger.kenbot.gui.MainFrame;
import kaendfinger.kenbot.listeners.CacheUpdater;
import kaendfinger.kenbot.listeners.CommandListener;
import kaendfinger.kenbot.listeners.EventListener;
import kaendfinger.kenbot.modules.ModuleLoader;
import org.pircbotx.PircBotX;
import org.pircbotx.hooks.managers.ListenerManager;
import org.pircbotx.hooks.managers.ThreadedListenerManager;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

public class KenBot {

    public static void main(final String[] args) throws Exception {
        Log.initialize();

        for (String s : args) {
            if (s.equals("-gui"))
                startGUI();
        }

        KenBotConfig.initializeConfiguration();
        registerCommands();
        ModuleLoader.initialize();

        ListenerManager<PircBotX> manager = new ThreadedListenerManager<>();
        manager.addListener(new CommandListener());
        manager.addListener(new EventListener());
        manager.addListener(new Console());
        BotUtils.getBot().setListenerManager(manager);

        BotUtils.getBot().setName(KenBotConfig.get("user"));

        String realname = KenBotConfig.get("realname");
        if (!realname.isEmpty())
            BotUtils.getBot().setVersion(realname);

        String identity = KenBotConfig.get("identity");
        if (!realname.isEmpty())
            BotUtils.getBot().setLogin(identity);

        BotUtils.getBot().useShutdownHook(true);
        BotUtils.getBot().setMessageDelay(0);

        BotUtils.connect();
        BotUtils.join();
    }

    private static void startGUI() {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame();
            }
        });
    }

    private static void registerCommands() {
        BotRegistry.register(new ModuleCommands());
        BotRegistry.register(new AdminCommands());
        BotRegistry.register(new UserCommands());
        BotRegistry.register(new ConfigCommands());

        BotRegistry.register(new CacheUpdater());
    }

    public static synchronized void restart() {
        try {
            BotUtils.disconnect();
        } catch (Exception e) {
            // Do nothing
        }

        try {
            final String separator = System.getProperty("file.separator");
            final String javaBin = System.getProperty("java.home") + separator + "bin" + separator + "java";
            final File currentJar = new File(KenBot.class.getProtectionDomain().getCodeSource().getLocation().toURI());

            if (!currentJar.getName().endsWith(".jar"))
                return;

            final ArrayList<String> command = new ArrayList<>();
            command.add(javaBin);
            command.add("-jar");
            command.add(currentJar.getPath());

            final ProcessBuilder builder = new ProcessBuilder(command);
            builder.start();
        } catch (URISyntaxException | IOException e) {
            System.out.println("Error restarting the bot");
        }

        System.exit(0);
    }

}
