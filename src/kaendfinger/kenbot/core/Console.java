package kaendfinger.kenbot.core;

import kaendfinger.kenbot.api.Log;
import org.pircbotx.hooks.ListenerAdapter;
import org.pircbotx.hooks.events.ActionEvent;
import org.pircbotx.hooks.events.MessageEvent;

public class Console extends ListenerAdapter {

    @Override
    public void onMessage(MessageEvent event) throws Exception {
        String message = "<" + event.getChannel().getName() + "><" + event.getUser().getNick() + "> " + event.getMessage();
        Log.log("log/" + event.getChannel().getName(), message);
    }

    @Override
    public void onAction(ActionEvent event) throws Exception {
        String message = "<" + event.getChannel().getName() + "> * " + event.getUser().getNick() + " " + event.getMessage();
        Log.log("log/" + event.getChannel().getName(), message);
    }

}