package kaendfinger.kenbot.core;

import java.util.HashMap;

public class UserCache {

    private static HashMap<String, String> nickServMappings = new HashMap<>();

    public static void addMapping(String nick, String nickServRegistered) {
        if (nickServMappings.containsKey(nick))
            nickServMappings.remove(nick);
        nickServMappings.put(nick, nickServRegistered);
    }

    public static void removeMapping(String nick) {
        nickServMappings.remove(nick);
    }

    public static String getNickServName(String nick) {
        return nickServMappings.get(nick);
    }

    public static boolean isCached(String nick) {
        return nickServMappings.containsKey(nick);
    }

}
