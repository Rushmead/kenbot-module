package kaendfinger.kenbot.core;

import kaendfinger.kenbot.api.configuration.Configuration;
import kaendfinger.kenbot.api.configuration.Property;

import java.io.File;

public class KenBotConfig {

    private static Configuration config = new Configuration(new File("bot.conf"));
    private static boolean initialized;

    public static void initializeConfiguration() {
        if (initialized) return;

        config.header("Global configuration options");
        config.comment("Note: channel specific options in here are ignored");
        config.newLine();

        config.get(new Property("server", "irc.esper.net").addComment("Server to connect to"));
        config.get(new Property("user", "KenBotUser").addComment("User to connect as"));

        config.get(new Property("realname").addComment("Real name to connect as"));
        config.get(new Property("identity").addComment("Identity to connect as"));
        config.get(new Property("password").addComment("Password to supply to nickserv for identification"));

        config.get(new Property("verbose.invalid.command", "true").addComment("Whether to show an error message if a command not valid (valid options are true or false)"));
        config.get(new Property("verbose.invalid.permission", "true").addComment("Whether to show an error message when lacking permissions (valid options are true or false)"));

        config.get(new Property("force.registered", "false").addComment("Force user to be nickserv registered and logged in to use any commands"));

        config.get(new Property("channels", "#kaendfinger").addComment("Channel(s) to connect to; separated by a space"));
        config.get(new Property("modules").addComment("Module(s) to load on startup; separated by a space"));

        config.header("Global and per-channel configuration options");
        config.newLine();

        config.get(new Property("prefix", "!").addComment("Command character prefix to use commands for the bot"));

        config.header("Permissions");
        config.get(new Property("permissions").addComment("Permissions will always default to a denied state unless otherwise defined")
                .addComment("Permission supports the following types: ")
                .addComment("per-user (permission per user, per channel)")
                .addComment("per-user global (permission per user, command not restricted by channel)")
                .addComment("per-channel (these are global for any user of the channel, but cannot be used in other channels)")
                .addComment("Asterisk(\"*\") is a wildcard meaning *all permissions*")
                .addComment("\"An example of an owner using this is:> permissions.<your name>: *\"")
                .addComment("Permissions are separated by spaces to add numerous commands")
                .addComment("Permission definitions are just commands")
                .addComment("Global format:> permissions: <command> <command 2> <...>")
                .addComment("Per user format:> permissions.<channel>.<user>: <commands...>")
                .addComment("Per user global format:> permissions.<user>: <commands...>")
                .addComment("Per channel format:> permissions.<channel>: <commands...>")
                .addComment("To negate permissions, prefix the command you want to make sure they don't have permissions for with a -"));

        config.newLine();
        config.header("Groups");
        config.comment("To make a group option add group.<groupname>: <value>");
        config.newLine();

        config.lock();
        initialized = true;
    }

    public static String get(String node) {
        if (config == null) {
            System.out.println("Attempted to get configuration variable while reloading");
            return null;
        }
        return config.get(node);
    }

    public static void set(String defaultValue, String option, String... options) {
        config.set(defaultValue, option, options);
    }

    public static synchronized void reload() {
        config = null;
        config = new Configuration(new File("bot.conf"));
        config.lock();
    }


}
