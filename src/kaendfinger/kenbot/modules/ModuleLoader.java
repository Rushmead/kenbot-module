package kaendfinger.kenbot.modules;

import kaendfinger.kenbot.api.IModule;
import kaendfinger.kenbot.api.Log;
import kaendfinger.kenbot.api.utils.StringUtils;
import kaendfinger.kenbot.core.KenBotConfig;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class ModuleLoader extends Module {

    private ArrayList<File> files = new ArrayList<>();

    public static void initialize() {
        ModuleLoader loader = new ModuleLoader();
        loader.setupModules();
    }

    public void setupModules() {
        createDir();
        getModuleFiles();
        ArrayList<String> classes = new ArrayList<>();
        ArrayList<URL> urls = new ArrayList<>();
        for (File f : files) {
            Manifest mf = getManifest(f);
            String moduleClass = mf.getMainAttributes().getValue("Module-class");
            if (moduleClass == null) {
                System.out.println("Error: Attribute \"Module-class\" is missing in " + f.getName() + ", skipping");
                continue;
            }
            try {
                urls.add(f.toURI().toURL());
                classes.add(moduleClass);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }

        }
        loader = new URLClassLoader(urls.toArray(new URL[urls.size()]), this.getClass().getClassLoader());
        for (String c : classes) {
            try {
                Class mc = Class.forName(c, true, loader);
                IModule module = (IModule) mc.newInstance();
                if (Module.modules.get(module.name()) != null)
                    throw new RuntimeException("Duplicate modules detected: " + module.name());
                Module.modules.put(module.name(), new Module(module));
            } catch (Exception e) {
                throw new RuntimeException("Error loading module", e);
            }
        }
        loadModulesFromConfig();
    }

    private void loadModulesFromConfig() {
        for (String s : StringUtils.getQuoted(KenBotConfig.get("modules"))) {
            Module module = ModuleLoader.getModules().get(s);
            if (module == null) {
                Log.log("Module \"" + s + "\" is non-existant");
                System.exit(0);
            }
            module.load();
        }
    }

    private Manifest getManifest(File f) {
        try {
            JarFile jf = new JarFile(f);
            return jf.getManifest();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private void getModuleFiles() {
        try {
            FileVisitor<Path> visitor = new FileProcessor(this);
            Files.walkFileTree(Paths.get("modules"), visitor);
            if (files.size() == 1) {
                System.out.println(files.size() + " module has been identified");
            } else {
                System.out.println(files.size() + " modules have been identified");
            }

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void createDir() {
        File f = new File("modules");
        if (!f.isDirectory()) {
            if (f.mkdir()) return;
            System.out.println("Unable to create a modules folder, please manually create one, continuing anyways...");
        }
    }

    private static final class FileProcessor extends SimpleFileVisitor<Path> {

        public ModuleLoader instance;

        public FileProcessor(ModuleLoader instance) {
            this.instance = instance;
        }

        @Override
        public FileVisitResult visitFile(Path path, BasicFileAttributes attributes) throws IOException {
            if (path.toString().endsWith(".jar")) {
                instance.files.add(path.toFile());
            }
            return FileVisitResult.CONTINUE;
        }
    }

}
