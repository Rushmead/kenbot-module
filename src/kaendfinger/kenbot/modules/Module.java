package kaendfinger.kenbot.modules;

import kaendfinger.kenbot.api.IModule;
import kaendfinger.kenbot.api.events.ConfigurationEvent;
import kaendfinger.kenbot.api.registry.BotRegistry;

import java.net.URLClassLoader;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Module {

    protected static HashMap<String, Module> modules = new HashMap<>();
    protected static URLClassLoader loader;

    public IModule module;

    private String name;
    private boolean loaded;

    protected Module() {
    }

    public Module(IModule module) {
        this.module = module;
        name = module.name();
        loaded = false;

        module.setup(new ConfigurationEvent(name.replaceAll(" ", "_") + ".conf"));
    }

    public static Map<String, Module> getModules() {
        return Collections.unmodifiableMap(modules);
    }

    public String getName() {
        return name;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public boolean load() {
        if (isLoaded()) return false;
        try {
            if (module.classes() != null) {
                for (String classes : module.classes()) {
                    Class toRegister = Class.forName(classes, true, loader);
                    Object o = toRegister.newInstance();
                    BotRegistry.register(o);
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        module.load();
        loaded = true;
        return true;
    }

    public boolean unload() {
        if (!isLoaded()) return false;
        try {
            for (String classes : module.classes()) {
                Class toUnregister = Class.forName(classes, true, loader);
                Object o = toUnregister.newInstance();
                BotRegistry.unregister(o);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        module.unload();
        loaded = false;
        return true;
    }

}
