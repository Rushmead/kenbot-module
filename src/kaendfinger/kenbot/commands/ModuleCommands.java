package kaendfinger.kenbot.commands;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.modules.Module;
import kaendfinger.kenbot.modules.ModuleLoader;
import org.pircbotx.hooks.events.MessageEvent;

public class ModuleCommands {

    @Command(help = "Shows the loaded modules")
    public static void loaded(MessageEvent event) {
        String modules = "Modules loaded: ";
        for (Module module : ModuleLoader.getModules().values()) {
            if (module.isLoaded())
                modules += "\"" + module.getName() + "\" ";
        }
        event.getBot().sendNotice(event.getUser(), modules);
    }

    @Command(help = "Shows the unloaded modules")
    public static void unloaded(MessageEvent event) {
        String modules = "Modules unloaded: ";
        for (Module module : ModuleLoader.getModules().values()) {
            if (!module.isLoaded())
                modules += "\"" + module.getName() + "\" ";
        }
        event.getBot().sendNotice(event.getUser(), modules);
    }

    @Command(help = "Loads a specified module", usage = "load <module>", minArgs = 1)
    public static void load(MessageEvent event, String[] args) {
        String full = "";
        for (String s : args)
            full += s + " ";
        full = full.trim();

        Module module = ModuleLoader.getModules().get(full);
        if (module == null) {
            event.getBot().sendNotice(event.getUser(), "Error: Module \"" + full + "\" is non-existant");
            return;
        }
        if (!module.load()) {
            event.getBot().sendNotice(event.getUser(), "Error: Module \"" + full + "\" is already loaded");
            return;
        }
        event.getBot().sendNotice(event.getUser(), "Successfully loaded: " + full);
    }

    @Command(help = "Unloads a specified module", usage = "unload <module>", minArgs = 1)
    public static void unload(MessageEvent event, String[] args) {
        String full = "";
        for (String s : args)
            full += s + " ";
        full = full.trim();

        Module module = ModuleLoader.getModules().get(full);
        if (module == null) {
            event.getBot().sendNotice(event.getUser(), "Error: Module \"" + full + "\" is non-existant");
            return;
        }
        if (!module.unload()) {
            event.getBot().sendNotice(event.getUser(), "Module \"" + full + "\" is already unloaded");
            return;
        }
        event.getBot().sendNotice(event.getUser(), "Successfully unloaded: " + full);
    }

    @Command(help = "Reloads a specified module, shortcut for calling the unload and load command", usage = "reload <module>", minArgs = 1)
    public static void reloadModule(MessageEvent event, String[] args) {
        unload(event, args);
        load(event, args);
    }

}
