package kaendfinger.kenbot.commands;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.core.KenBot;
import org.pircbotx.hooks.events.MessageEvent;

public class AdminCommands {

    @Command(help = "Restarts the entire bot")
    public static void reset(MessageEvent event) {
        KenBot.restart();
    }

    @Command(help = "Ops a given user", usage = "op <user>", minArgs = 1, maxArgs = 1)
    public static void op(MessageEvent event, String[] args) {
        if (!event.getChannel().isOp(event.getUser())) {
            event.getBot().sendNotice(event.getUser(), "You do not have permission to do this on this channel.");
            return;
        }   
        event.getBot().op(event.getChannel(), event.getBot().getUser(args[0]));
    }

    @Command(help = "Deops a given user", usage = "deop <user>", minArgs = 1, maxArgs = 1)
    public static void deop(MessageEvent event, String[] args) {
        if (!event.getChannel().isOp(event.getUser())) {
            event.getBot().sendNotice(event.getUser(), "You do not have permission to do this on this channel.");
            return;
        }
        event.getBot().deOp(event.getChannel(), event.getBot().getUser(args[0]));
    }

    @Command(help = "Voices a given user", usage = "voice <user>", minArgs = 1, maxArgs = 1)
    public static void voice(MessageEvent event, String[] args) {
        if (!event.getChannel().isOp(event.getUser())) {
            event.getBot().sendNotice(event.getUser(), "You do not have permission to do this on this channel.");
            return;
        }
        event.getBot().voice(event.getChannel(), event.getBot().getUser(args[0]));
    }

    @Command(help = "Devoices a given user", usage = "devoice <user>", minArgs = 1, maxArgs = 1)
    public static void devoice(MessageEvent event, String[] args) {
        if (!event.getChannel().isOp(event.getUser())) {
            event.getBot().sendNotice(event.getUser(), "You do not have permission to do this on this channel.");
            return;
        }
        event.getBot().deVoice(event.getChannel(), event.getBot().getUser(args[0]));
    }

    @Command(help = "Kicks a given user in a fun way", usage = "slay <user>", minArgs = 1, maxArgs = 1)
    public static void slay(MessageEvent event, String[] args) {
        if (!event.getChannel().isOp(event.getUser())) {
            event.getBot().sendNotice(event.getUser(), "You do not have permission to do this on this channel.");
            return;
        }
        event.getBot().kick(event.getChannel(), event.getBot().getUser(args[0]), "You just got slain");
    }

    @Command(help = "Joins a given channel", usage = "join <channel>", minArgs = 1, maxArgs = 1)
    public static void join(MessageEvent event, String[] args) {
        event.getBot().joinChannel(args[0]);
    }

    @Command(help = "Parts a given channel", usage = "part [channel]", maxArgs = 1)
    public static void part(MessageEvent event, String[] args) {
        if (args.length == 1) {
            event.getBot().partChannel(event.getBot().getChannel(args[0]));
        } else {
            event.getBot().partChannel(event.getChannel());
        }
    }

}
