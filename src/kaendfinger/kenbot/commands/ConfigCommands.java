package kaendfinger.kenbot.commands;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.core.KenBotConfig;
import org.pircbotx.hooks.events.MessageEvent;

public class ConfigCommands {

    @Command(help = "Reload the configuration")
    public static void reloadConfig(MessageEvent event) {
        KenBotConfig.reload();
        event.getBot().sendNotice(event.getUser(), "KenBot configuration successfully reloaded");
    }

    @Command(help = "Control the configuration", usage = "config <get|set|append>", minArgs = 1)
    public static void config(MessageEvent event, String[] args) {
        switch (args[0]) {
            case "get":

                if (args.length != 2) {
                    event.getBot().sendNotice(event.getUser(), "Usage: config get <node>");
                    return;
                }
                if (args[1].equals("password")) {
                    event.getBot().sendNotice(event.getUser(), "Error: sensitive information is not allowed to be displayed");
                    return;
                }
                String value = KenBotConfig.get(args[1]);
                if (value.isEmpty()) {
                    event.getBot().sendNotice(event.getUser(), "Error: no configuration option detected in the configuration");
                    return;
                }
                event.getBot().sendNotice(event.getUser(), value);
                return;
            case "set":
                if (args.length < 3) {
                    event.getBot().sendNotice(event.getUser(), "Usage: config set <node> <value>");
                    return;
                }
                if (args[1].equals("password")) {
                    event.getBot().sendNotice(event.getUser(), "Error: sensitive information is not allowed to be controlled");
                    return;
                }
                String full = "";
                for (int i = 2; i < args.length; i++) {
                    if (i==2) {
                        full += args[i];
                    } else {
                        full += " " + args[i];
                    }
                }
                KenBotConfig.set(args[1], full);
                event.getBot().sendNotice(event.getUser(), "Config Option: " + args[1] + " set to \"" + full + "\"");
                break;
            case "append":
                if (args.length < 3) {
                    event.getBot().sendNotice(event.getUser(), "Usage: config set <node> <value to add>");
                    return;
                }
                if (args[1].equals("password")) {
                    event.getBot().sendNotice(event.getUser(), "Error: sensitive information is not allowed to be controlled");
                    return;
                }
                value = KenBotConfig.get(args[1]);
                full = "";
                for (int i = 2; i < args.length; i++)
                    full += " " + args[i];
                KenBotConfig.set((value + full).trim(), args[1]);
                break;
            default:
                try {
                    event.getBot().sendNotice(event.getUser(), ConfigCommands.class.getMethod("config").getAnnotation(Command.class).usage());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return;
        }
        event.getBot().sendNotice(event.getUser(), "Configuration successfully updated");
    }

}
