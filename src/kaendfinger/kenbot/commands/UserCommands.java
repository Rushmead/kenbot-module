package kaendfinger.kenbot.commands;

import kaendfinger.kenbot.api.registry.Command;
import kaendfinger.kenbot.api.utils.BotUtils;
import kaendfinger.kenbot.listeners.CommandListener;
import org.pircbotx.hooks.events.MessageEvent;

public class UserCommands {

    @Command(help = "Gets help for a given command or shows all commands you have access to", usage = "help [command]", maxArgs = 1)
    public static void help(MessageEvent event, String[] args) {
        if (args.length == 0) {

            String msg = "Available commands:";
            for (String command : CommandListener.getCommands().keySet()) {
                if (BotUtils.hasPermission(command, event.getChannel().getName(), event.getUser().getNick())) {
                    msg += " " + command;
                }
            }
            event.getBot().sendNotice(event.getUser(), msg);
            return; // Don't want to execute the other help
        }

        String command = args[0].trim();

        if (CommandListener.commandExists(command)) {
            if (BotUtils.hasPermission(command, event.getChannel().getName(), event.getUser().getNick())) {
                String help = CommandListener.getCommands().get(command).getAnnotation(Command.class).help();
                if (help.isEmpty()) {
                    BotUtils.getBot().sendNotice(event.getUser(), "No help available for this command");
                } else {
                    BotUtils.getBot().sendNotice(event.getUser(), help);
                }
            } else {
                event.getBot().sendNotice(event.getUser(), "Error: lacking permission to get help for " + command);
            }

        } else {
            event.getBot().sendNotice(event.getUser(), "Error: \"" + command + "\" is not a valid command");
        }

    }

}
