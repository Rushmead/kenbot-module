package kaendfinger.kenbot.api.events;

import kaendfinger.kenbot.api.configuration.Configuration;

import java.io.File;

public class ConfigurationEvent {

    private File configurationPath;
    private Configuration config;

    public ConfigurationEvent(String path) {
        File configDir = new File("config");
        if (!configDir.isDirectory())
            if (!configDir.mkdir())
                System.out.println("Unable to make the config directory, please make one");
        configurationPath = new File("config/", path);
    }

    @SuppressWarnings("unused")
    public Configuration getConfiguration() {
        if (config == null)
            config = new Configuration(configurationPath);
        return config;
    }


}
