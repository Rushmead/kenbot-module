package kaendfinger.kenbot.api;

import kaendfinger.kenbot.gui.TextArea;

import java.io.*;

public class Log {

    private static PrintStream systemOut;
    private static boolean initialized = false;

    /**
     * Logs {@code s} to the console and logs
     *
     * @param s string to log
     */
    public static void log(String s) {
        log(s, true);
    }

    /**
     * Logs {@code s} to the logs and optionally the console
     *
     * @param s       string to log
     * @param console whether to log to console or not
     */
    public static void log(String s, boolean console) {
        if (console) {
            systemOut.println(s);
            logGUI(s);
        }
        LogManager.write(s);
    }

    /**
     * Logs {@code s} to the console and logs
     *
     * @param file name of the file to log {@code s} to
     * @param s    string to log
     */
    public static void log(String file, String s) {
        log(file, s, true);
    }

    /**
     * Logs {@code s} to the logs and optionally the console
     *
     * @param file    name of the file to log {@code s} to
     * @param s       string to log
     * @param console whether to log to console or not
     */
    public static void log(String file, String s, boolean console) {
        if (console) {
            systemOut.println(s);
            logGUI(s);
        }
        LogManager.write(file, s);
    }

    /**
     * Logs {@code s} to the console only
     *
     * @param s string to log
     */
    public static void console(String s) {
        systemOut.println(s);
    }

    /**
     * Initializes the logger
     * KenBot itself calls this, there is no reason to call this in modules
     */
    public static void initialize() {
        if (initialized) return;
        initialized = true;
        systemOut = System.out;
        System.setOut(new DummyPrintStream());
    }

    /**
     * Logs to the GUI only
     *
     * @param s string to log
     */
    private static void logGUI(String s) {
        if (TextArea.textArea != null) {
            TextArea.textArea.append(s + "\n");
            TextArea.textArea.setCaretPosition(TextArea.textArea.getDocument().getLength());
        }
    }

    private static class LogManager {

        private static boolean dirChecked = false;

        private static void checkLogDir() {
            if (dirChecked) return;
            File dir = new File("log");
            if (!dir.exists())
                if (!dir.mkdir())
                    System.out.println("Error creating log directory, please manually create one");
            dirChecked = true;
        }

        public static void write(String s) {
            write("bot.log", s);
        }

        public static void write(String file, String s) {
            checkLogDir();
            File f = new File(file);
            try {
                BufferedWriter out = new BufferedWriter(new FileWriter(f, true), 32768);
                out.write(s + "\n");
                out.close();
            } catch (Exception e) {
                systemOut.println("Unable to log to " + file + " with reason " + e.getMessage());
                e.printStackTrace();
            }
        }

    }

    private static class DummyPrintStream extends PrintStream {

        public DummyPrintStream() {
            super(new DummyOutputStream());
        }

        @Override
        public void println(String s) {
            Log.log(s);
        }

        @Override
        public void print(String s) {
            println(s);
        }
    }

    private static class DummyOutputStream extends OutputStream {

        @Override
        public void write(int b) throws IOException {
        }

    }

}
