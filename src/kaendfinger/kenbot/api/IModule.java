package kaendfinger.kenbot.api;

import kaendfinger.kenbot.api.events.ConfigurationEvent;

/**
 * Make sure to point the "Module-class" attribute in your MANIFEST.MF file to the class that implements this interface
 * e.g. usage: Module-class: my.Module where Module implements IModule
 */
public interface IModule {

    /**
     * Used to setup the module including configuration options
     */
    public void setup(ConfigurationEvent event);

    /**
     * Kept for more flexibility.
     * Do not register/unregister classes (BotRegistry) unless you know what you're doing.
     */
    public void load();

    /**
     * Kept for more flexibility.
     * Do not register/unregister classes (BotRegistry) unless you know what you're doing.
     */
    public void unload();

    /**
     * The array of strings of classes will automatically be registered and unregistered upon loading/unloading
     *
     * @return classes to be registered based on fully qualified names
     */
    public String[] classes();

    /**
     * @return name of the Module
     */
    public String name();


}
