package kaendfinger.kenbot.api.registry;

import kaendfinger.kenbot.api.Log;
import kaendfinger.kenbot.api.utils.MethodUtils;
import kaendfinger.kenbot.listeners.CommandListener;
import kaendfinger.kenbot.listeners.EventListener;
import org.pircbotx.hooks.events.MessageEvent;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

public class BotRegistry {

    /**
     * Register an instance of a class so your commands and event listeners will be registered
     * Only use this method if you know what your doing and the class isn't automatically registered
     *
     * @param instance An instance of a class
     */
    public static void register(Object instance) {
        for (Method method : instance.getClass().getDeclaredMethods()) {
            if (!Modifier.isStatic(method.getModifiers())) continue;
            if (!method.isAccessible()) method.setAccessible(true);

            if (method.isAnnotationPresent(Command.class)) {
                MethodUtils.paramEquals(0, method, instance, MessageEvent.class, true);
                MethodUtils.paramEquals(1, method, instance, String[].class, false);
                CommandListener.addCommand(method);
            } else if (method.isAnnotationPresent(kaendfinger.kenbot.api.registry.EventListener.class)) {
                try {
                    String handlerName = method.getAnnotation(kaendfinger.kenbot.api.registry.EventListener.class).name();
                    String eventName = method.getAnnotation(kaendfinger.kenbot.api.registry.EventListener.class).event();
                    eventName = EventListener.eventPrefix + eventName;
                    MethodUtils.paramEquals(0, method, instance, Class.forName(eventName), true);
                    EventListener.addEvent(eventName, handlerName, method);
                } catch (ClassNotFoundException e) {
                    Log.console("Unable to find class: " + e.getMessage());
                }

            }
        }
    }

    /**
     * Unregister an instance of a class so your commands and event listeners will no longer be registered
     * Only use this method if you know what your doing and the class isn't automatically unregistered
     *
     * @param instance An instance of a class
     */
    public static void unregister(Object instance) {
        for (Method method : instance.getClass().getDeclaredMethods()) {
            if (!Modifier.isStatic(method.getModifiers())) continue;
            if (!method.isAccessible()) method.setAccessible(true);

            if (method.isAnnotationPresent(Command.class)) {
                CommandListener.removeCommand(method.getName());
            } else if (method.isAnnotationPresent(kaendfinger.kenbot.api.registry.EventListener.class)) {
                String handlerName = method.getAnnotation(kaendfinger.kenbot.api.registry.EventListener.class).name();
                String eventName = method.getAnnotation(kaendfinger.kenbot.api.registry.EventListener.class).event();
                EventListener.removeEvent(eventName, handlerName);
            }
        }
    }


}
