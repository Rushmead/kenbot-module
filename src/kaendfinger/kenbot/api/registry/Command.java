package kaendfinger.kenbot.api.registry;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@literal @}Command will register the method as a command, the method name becomes the command name <br />
 * Access Modifiers: Any is allowed <br />
 * Parameters: (MessageEvent) <br />
 * Return: void <br />
 *
 * @see kaendfinger.kenbot.api.registry.BotRegistry#register(Object) Registering classes that contain @Command
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface Command {

    /**
     * @return String a description of the command
     */
    String help() default "";

    /**
     * Replied to user if the minimum or maximum arguments check fails
     *
     * @return how to use the command
     */
    String usage() default "";

    /**
     * -1 means ignore minArgs requirement
     *
     * @return minimum number of arguments to call the command
     */
    int minArgs() default -1;

    /**
     * -1 means ignore maxArgs requirement
     *
     * @return maximum number of arguments to call the command
     */
    int maxArgs() default -1;

}
