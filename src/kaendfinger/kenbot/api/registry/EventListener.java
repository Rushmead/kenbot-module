package kaendfinger.kenbot.api.registry;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * {@literal @}EventListener will register the method as an event listener<br />
 * Access Modifiers: Any is allowed <br />
 * Parameters: Depends on the event you choose to listen to <br />
 * Return: void <br />
 *
 * @see kaendfinger.kenbot.api.registry.BotRegistry#register(Object) Registering classes that contain @EventListener
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface EventListener {

    /**
     * @return name of the event listener
     */
    String name();

    /**
     * The name of the event is the exact required parameter of the method
     *
     * @return name of the event to be registered for the method
     */
    String event();

}
