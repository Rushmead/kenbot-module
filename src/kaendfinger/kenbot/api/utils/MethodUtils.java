package kaendfinger.kenbot.api.utils;

import java.lang.reflect.Method;

public class MethodUtils {

    public static void paramEquals(int index, Method method, Object o, Class<?> clazz, boolean force) {
        if (method.getParameterTypes().length >= (index + 1)) {
            if (!(method.getParameterTypes()[index].equals(clazz))) {
                String error = "Method \"%s\" in class \"%s\" has a parameter (param index: #%d) that does not equal %s";
                throw new RuntimeException(String.format(error, o.getClass().getName(), method.getName(), index, clazz.getName()));
            }
        } else {
            if (force) {
                String error = "Method \"%s\" in class \"%s\" does not have a parameter (param index: #%d) that equals %s";
                throw new RuntimeException(String.format(error, o.getClass().getName(), method.getName(), index, clazz.getName()));
            }
        }
    }
}
