package kaendfinger.kenbot.api.utils;

import kaendfinger.kenbot.core.BotExtender;
import kaendfinger.kenbot.core.KenBotConfig;
import kaendfinger.kenbot.core.UserCache;
import kaendfinger.kenbot.listeners.CommandListener;
import org.pircbotx.hooks.events.WhoisEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

public class BotUtils {

    private static final BotExtender bot = new BotExtender();

    /**
     * Gets the BotExtender object
     *
     * @return bot BotExtender object
     */
    public static BotExtender getBot() {
        return bot;
    }

    /**
     * Grabs the registered name of a user if they are logged in
     *
     * @param nick user nickname
     * @return logged in name
     */
    public static String registeredName(String nick) {
        if (UserCache.isCached(nick))
            return UserCache.getNickServName(nick);

        try {
            BotUtils.getBot().sendRawLine("WHOIS " + nick);
            WhoisEvent event = BotUtils.getBot().waitFor(WhoisEvent.class);
            String registeredName = event.getRegisteredAs();

            boolean isRegistered = (event.getRegisteredAs() != null && !event.getRegisteredAs().isEmpty());
            if (KenBotConfig.get("force.registered").equalsIgnoreCase("true")) {
                UserCache.addMapping(nick, isRegistered ? registeredName : null);
                return (isRegistered ? registeredName : null);
            }

            UserCache.addMapping(nick, registeredName);
            return registeredName;
        } catch (InterruptedException ex) {
            throw new RuntimeException("Couldn't finish querying user for verified status", ex);
        }
    }

    /**
     * Check if {@code user} has permission to use a command
     *
     * @param command command to check for permissions
     * @param channel channel to check for channel specific permissions
     * @param nick    users to check for user specific permissions
     * @return true if user has permission, else false
     */
    public static boolean hasPermission(String command, String channel, String nick) {
        if (!CommandListener.commandExists(command))
            return false;

        String registeredName = registeredName(nick);
        if ((KenBotConfig.get("force.registered").equalsIgnoreCase("true")) && (registeredName == null))
            return false;

        ArrayList<String> permissions = new ArrayList<>();
        permissions.addAll(Arrays.asList(KenBotConfig.get("permissions").trim().split(" ")));
        permissions.addAll(Arrays.asList(KenBotConfig.get("permissions." + registeredName).trim().split(" ")));
        permissions.addAll(Arrays.asList(KenBotConfig.get("permissions." + channel).trim().split(" ")));
        permissions.addAll(Arrays.asList(KenBotConfig.get("permissions." + channel + "." + registeredName).trim().split(" ")));

        return !(permissions.contains("-" + command) || permissions.contains("-*")) && (permissions.contains(command) || permissions.contains("*"));

    }

    /**
     * Join the default channels from the configuration file all at once
     */
    public static void join() {
        for (String s : KenBotConfig.get("channels").split(" ")) {
            BotUtils.getBot().joinChannel(s.trim());
        }
    }

    /**
     * Connect to server
     *
     * @throws Exception Unable to connect or possible already connected
     */
    public static void connect() throws Exception {
        boolean connected = false;
        int attempt = 0;

        while (!connected) {
            attempt++;
            try {
                getBot().connect(KenBotConfig.get("server"));
                connected = true;
            } catch (Exception e) {
                System.out.println("Connection attempt #" + attempt + " to server failed");
                try {
                    TimeUnit.SECONDS.sleep(10);
                } catch (InterruptedException ex) {
                    // Do nothing
                }
            }
        }

    }

    /**
     * Disconnect from server
     *
     * @throws Exception Already disconnected
     */
    public static void disconnect() throws Exception {
        getBot().disconnect();
    }

}