package kaendfinger.kenbot.api.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {

    /**
     * @param s string to extract the args from
     * @return array of double quoted, single quoted, and unquoted strings (quotations filtered) from {@code s}
     */
    public static String[] getQuoted(String s) {
        List<String> matchList = new ArrayList<>();
        Pattern regex = Pattern.compile("[^\\s\"']+|\"([^\"]*)\"|'([^']*)'");
        Matcher regexMatcher = regex.matcher(s);
        while (regexMatcher.find())
            if (regexMatcher.group(1) != null)
                matchList.add(regexMatcher.group(1));
            else if (regexMatcher.group(2) != null)
                matchList.add(regexMatcher.group(2));
            else
                matchList.add(regexMatcher.group());
        return matchList.toArray(new String[matchList.size()]);
    }

}
