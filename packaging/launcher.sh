#!/bin/sh
if [ ! -d "${HOME}/.kenbot/" ]
then
  mkdir ${HOME}/.kenbot/ -p
fi
echo "Starting KenBot..."
[ ! -f "~/.kenbot/KenBot.jar" ] && cp /usr/share/KenBot/KenBot.jar ~/.kenbot/KenBot.jar
cd ~/.kenbot/
java -jar KenBot.jar ${@}
