# KenBot

Welcome to KenBot's Bitbucket repository!

## Getting Started

* Install [Apache Ant](http://ant.apache.org/bindownload.cgi) and JRE/JDK 7
* Run ant builds with target "jar"
* Add code into your own package and register with command registry.
* Build and deploy.

## Dependencies

* PircBotX (Ant will automatically download this)

## Making a module
* Read the IModule interface documentation in the API (kaendfinger.kenbot.api.IModule)

## How to use KenBot
* Launch KenBot: java -jar KenBot.jar
* Edit configuration in bot.conf
* Launch KenBot Again: java -jar KenBot.jar

## Arch Linux Package
> We are in [AUR](https://aur.archlinux.org/packages/kenbot-git/) via the package "kenbot-git"
>
> Dependencies for build: "jdk7-openjdk", "apache-ant"
>
> The PKGBUILD will clone the Git repository and run Ant.